$fn = 64;
length = 99;
margin = 0.5;
wall = 4.44;

baseheight = 4;
baseframe = 7;
basecorner = 17;
basecornerradius = (basecorner - baseframe) / (sqrt(2) - 1);
topcornerradius = 40 / 2;
epsilon = 0.01;
cornerdrill = 3.81;
cornerheight = 3;
pcbthick = 2.0; //1.6;
drilldiameter = 3.2;
wallheightabovepcb = 25;
wallheightsub = baseheight + cornerheight + pcbthick;
totallength = length + 2 * (margin + wall);

//name should be: midiradius
mididiameter = (27.94 - 10.16) / 2;

module skrewbase() {
	cube([cornerdrill*2,cornerdrill*2, baseheight+cornerheight]);
}

module onewall() {
	cube([ wall, totallength, wallheightsub+wallheightabovepcb]);
}
module midihole() {
	translate([-epsilon,0,mididiameter+1])
		rotate([0,90,0])
			cylinder(10,mididiameter,mididiameter);
}
connectorradius = 4 / 2;
connectordistance = 0.5;
module connectorhole() {
	translate([-epsilon,-(connectorradius+connectordistance),connectorradius+connectordistance])
		rotate([0,90,0])
			cylinder(10,connectorradius,connectorradius);
	translate([-epsilon,(connectorradius+connectordistance),connectorradius+connectordistance])
		rotate([0,90,0])
			cylinder(10,connectorradius,connectorradius);
}
to220holeradius = 3.84 / 2;
to220height = 28.27 - (9.80 + 2.74);
module to220hole() {
	translate([-epsilon,0,to220height+to220holeradius])
		rotate([0,90,0])
			cylinder(10,to220holeradius,to220holeradius);
}

potiradius = ((1 / 4) * 25.4) / 2.0; //quarter inch
//see: https://www.vishay.com/docs/57054/248249.pdf
potiheight = 12.7;
potisubsurface = 0.6;

module potihole() {
	translate([-epsilon,0,potisubsurface+(potiheight/2)+potiradius])
		rotate([0,90,0])
			cylinder(10,potiradius,potiradius);
}

font = "Noto Sans CJK SC:style=Medium";
module midifont() {
//"IN 输入 Shūrù OUT 输出 输出";
	text = "+ -  in \u8f93\u5165    out \u8f93\u51fa ";
	translate([1,wall+70,3])
		scale(0.25)
			rotate([90,0,-90])
				linear_extrude(height = 5) {text(text = str(text), font = font, size = 20, halign=0);}
}
module frontfont() {
	// 合成
	text = "\u97f3\u91cf      DAC     \u97f3\u91cf  SID\u5408\u6210 ";
	translate([1,wall+87,3]) {
		scale(0.25)
			rotate([90,0,-90])
				linear_extrude(height = 5) {text(text = str(text), font = font, size = 20,halign=0);}
	}
	translate([1,wall+67,10]) {
		text = "+        -                +       - ";
		scale(0.25)
			rotate([90,0,-90])
				linear_extrude(height = 5) {text(text = str(text), font = font, size = 20,halign=0);}
	}
}

module sidefont() {
	text = "\u653e\u5927\u5668 ";
	translate([-1,wall+15,3]) {
		translate([wall,0,0])
			rotate([0,0,180])
				scale(0.25)
					rotate([90,0,-90])
					linear_extrude(height = 5) {text(text = str(text), font = font, size = 20,halign=0);}
	}
}

module wallA() {
	difference()
	{
		union() {
			onewall();
		}
		midifont();
		translate	([0,wall+margin,wallheightsub])
			union() {
				//translate
				translate([0,mididiameter+10.16,0])
					midihole();
				translate([0,mididiameter+39.370,0])
					midihole();
				translate([0,(71.120+60.960)/2,0])
					connectorhole();
			}
	}
}

module wallB() {
	difference()
	{
		union() {
			onewall();
		}
		sidefont();
		translate	([0,wall+margin,wallheightsub])
			union() {
				//translate
				translate([0,length-17.145,0])
					to220hole();
				translate([0,length-29.210,0])
					to220hole();
				translate([0,length-74.930,0])
					potihole();
				translate([0,length-87.630,0])
					to220hole();
			}
	}
}

module wallC() {
	difference()
	{
		union() {
			onewall();
		}
		frontfont();
		translate	([0,wall+margin,wallheightsub])
			union() {
				translate([0,35.56,0])
					potihole();
				translate([0,78.74,0])
					potihole();
				translate([0,(19.05+8.89)/2,0])
					connectorhole();
				translate([0,(62.30+52.07)/2,0])
					connectorhole();
			}
	}
}
//wallB();
module wallD() {
	onewall();
}
module baseonly() {
	difference()
	{
		union	() {
			translate([1.5,1.5,0])
				cube([totallength-3,totallength-3,baseheight]);
		}
		translate([0,0,-epsilon])
			union() {
				translate([wall+margin+baseframe+basecornerradius,wall+margin+basecornerradius+baseframe,0])
					cylinder(10,basecornerradius,basecornerradius);
				translate([totallength-(wall+margin+baseframe+basecornerradius),wall+margin+basecornerradius+baseframe,0])
					cylinder(10,basecornerradius,basecornerradius);
				translate([totallength-(wall+margin+baseframe+basecornerradius),totallength-(wall+margin+baseframe+basecornerradius),0])
					cylinder(10,basecornerradius,basecornerradius);
				translate([(wall+margin+baseframe+basecornerradius),totallength-(wall+margin+baseframe+basecornerradius),0])
					cylinder(10,basecornerradius,basecornerradius);
				translate([wall+margin+baseframe,wall+margin+basecornerradius+baseframe,-epsilon])
					cube([length-(2*baseframe),length-(2*basecornerradius+2*baseframe),baseheight+cornerheight]);
				
				translate([wall+margin+basecornerradius+baseframe,wall+margin+baseframe,-epsilon])
					cube([length-(2*basecornerradius+2*baseframe),length-(2*baseframe),baseheight+cornerheight]);
			}
	}
}

module toponly() {
	difference()
	{
		union	() {
			cube([totallength,totallength,baseheight]);
		}
		translate([0,0,-epsilon])
			union() {
				translate([wall+margin+baseframe+topcornerradius,wall+margin+topcornerradius+baseframe,0])
					cylinder(10,topcornerradius,topcornerradius);
				translate([totallength-(wall+margin+baseframe+topcornerradius),wall+margin+topcornerradius+baseframe,0])
					cylinder(10,topcornerradius,topcornerradius);
				translate([totallength-(wall+margin+baseframe+topcornerradius),totallength-(wall+margin+baseframe+topcornerradius),0])
					cylinder(10,topcornerradius,topcornerradius);
				translate([(wall+margin+baseframe+topcornerradius),totallength-(wall+margin+baseframe+topcornerradius),0])
					cylinder(10,topcornerradius,topcornerradius);
			}
	}
}

module sidcase() {
	difference()
	{
		union	() {
			baseonly();
			translate([0,0,wallheightsub+wallheightabovepcb])
				toponly();
			translate([wall+margin,wall+margin,0]) skrewbase();
			translate([totallength-(wall+margin+cornerdrill*2),wall+margin,0]) skrewbase();
			translate([totallength-(wall+margin+cornerdrill*2),totallength-(wall+margin+cornerdrill*2),0]) skrewbase();
			translate([wall+margin,totallength-(wall+margin+cornerdrill*2),0]) skrewbase();
			
			wallA();
			translate([totallength,totallength-wall,0])
				rotate([0,0,90])
					wallB();
			translate([totallength,totallength,0])
				rotate([0,0,-180])
					wallC();
			translate([totallength,0,0])
				rotate([0,0,-270])
					wallD();
	}
	translate([0,0,-epsilon])
		union() {
			translate([wall+margin+cornerdrill,wall+margin+cornerdrill,0])
				cylinder(200,drilldiameter/2.0,drilldiameter/2.0);
			translate([totallength-(wall+margin+cornerdrill),wall+margin+cornerdrill,0])
				cylinder(200,drilldiameter/2.0,drilldiameter/2.0);
			translate([wall+margin+cornerdrill,totallength-(wall+margin+cornerdrill),0])
				cylinder(200,drilldiameter/2.0,drilldiameter/2.0);
			translate([totallength-(wall+margin+cornerdrill),totallength-(wall+margin+cornerdrill),0])
				cylinder(200,drilldiameter/2.0,drilldiameter/2.0);
		}
	}
}

// here chose intersection or difference or

intersection() {
//rotate([180,0,0]) difference() {
	sidcase();

	union() {
		translate([-wall-epsilon,-wall-epsilon,-epsilon-(wall/2)])
			cube( [totallength,totallength,wallheightsub+wallheightabovepcb]);
		translate([-epsilon+(wall/2),(wall/2)-epsilon,-epsilon])
			cube( [totallength-(wall*2),totallength-(wall*2),wallheightsub+wallheightabovepcb]);

		translate([totallength/2,totallength/2,-epsilon])
			rotate([0,0,45])
				cylinder(wallheightabovepcb,totallength*0.75*sqrt(2),totallength*0.4*sqrt(2),$fn=4);
	}
}
